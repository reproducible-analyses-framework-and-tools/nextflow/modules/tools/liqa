#!/usr/bin/env nextflow

process liqa_refgene {
// Runs liqa index command
//
// input:
//   path fa - Reference FASTA
//   val parstr - Additional Parameters
//
// output:
//   tuple => emit: idx_files
//     path(fa) - Reference FASTA
//     path("${fa}*index") - Index Files

// require:
//   params.liqa$reference
//   params.liqa$liqa_index_parameters

  tag "${gtf}"
  label 'liqa_container'
  label 'liqa_refgene'
  cache 'lenient'
  storeDir "${params.shared_dir}/${fa}/liqa_refgene/"

  input:
  path fa
  val parstr

  output:
  tuple path("${gtf}"), path("${gtf}*refgene"), emit: idx_files

  script:
  """
  # Only supporting GTF indexing now.
  liqa -task refgene -ref ${ref} -format gtf -out ${gtf}.refgene
  """
}

process liqa_quantify {
// Runs liqa quantify (for quantifying from BAMs)
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(bam) - BAM
//   path(refgene_index) - Reference GTF Index
//   val parstr - Additoinal Parameters
//
// output:
//   tuple => emit: quants
//       val(pat_name) - Patient Name
//       val(run) - Run Name
//       val(dataset) - Dataset
//       path('*quant.sf') - Quant File

// require:
//   FQS
//   IDX_FILES
//   params.liqa$liqa_map_quant_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'liqa_container'
  label 'liqa_quantify'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${run}/liqa_quantify"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(fq1), path(fq2)
  path refgene_index
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("${dataset}-${pat_name}-${run}.quant.sf"), emit: quants

  script:
  """
  liqa -task quantify -refgene ${ref_gene_index} -bam ${bam} -out ${dataset}-${pat_name}-${run}.liqa_iso_exp # -max_distance <max distance> -f_weight <weight of F function>
  """
}

process liqa_novel {
// Runs liqa novel (for discovering novel transcripts)
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(bam) - BAM
//   path(refgene_index) - Reference GTF Index
//   val parstr - Additoinal Parameters
//
// output:
//   tuple => emit: quants
//       val(pat_name) - Patient Name
//       val(run) - Run Name
//       val(dataset) - Dataset
//       path('*quant.sf') - Quant File

// require:
//   FQS
//   IDX_FILES
//   params.liqa$liqa_map_quant_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'liqa_container'
  label 'liqa_novel'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${run}/liqa_novel"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(fq1), path(fq2)
  path refgene_index
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("${dataset}-${pat_name}-${run}.quant.sf"), emit: quants

  script:
  """
  liqa -task novel -refgene ${ref_gene_index} -bam ${bam} -out ${dataset}-${pat_name}-${run}.liqa_novel_isos # -num_cover 20 -num_support_read 10
  """
}
